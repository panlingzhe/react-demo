import React from 'react'
import { render } from 'react-dom'
import AppRouters from './routers'

render(
  <AppRouters />,
  document.getElementById('app')
)