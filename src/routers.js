import React, { Component } from 'react'

import { Router, Route, browserHistory } from 'react-router'

import HomePage from './views/home'
import LoginPage from './views/login'
import RegisterPage from './views/register'

class AppRouters extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={HomePage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
      </Router>   
    )
  }
}
export default AppRouters